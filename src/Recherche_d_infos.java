
import java.util.*;

public class Recherche_d_infos {


    static void camping(ArrayList<ArrayList<Object>> liste_personnes){
        for (ArrayList<Object> p : liste_personnes){
            if (p.get(0) == "Michel" || (int) p.get(2) >= 50){
                System.out.println("On a dans la liste quelqu'un qui adore le camping ");
                break;
            }
        }
    }

    static void meme_nb_lettres_nom_prenom(ArrayList<ArrayList<Object>> liste_personnes){
        for (ArrayList<Object> p : liste_personnes){
            if ((p.get(0).toString().length() == p.get(1).toString().length())){
                System.out.println("C'est formidable "+ p +" a "+p.get(0).toString().length()+ " lettres dans son nom et son prénom !");
            }
        }
    }

    static void meme_prenom(ArrayList<ArrayList<Object>> liste_personnes){
        ArrayList<String> liste_prenoms = new ArrayList<>();
        Set<String>set_prenoms = new HashSet<>();
        ArrayList<String> prenoms_en_double = new ArrayList<>();
        for (ArrayList<Object> p : liste_personnes){
            liste_prenoms.add(p.get(0).toString());
        }
        for (String p : liste_prenoms){
            if (!set_prenoms.add(p)) {
                prenoms_en_double.add(p);
            }
        }
        if (prenoms_en_double.size()>0) {
            for (String p : prenoms_en_double) {
                StringBuilder string_sortie = new StringBuilder(new String());
                for (ArrayList<Object> q : liste_personnes) {
                    if (Objects.equals(q.get(0).toString(), p)) {
                        string_sortie.append(q).append(" et ");
                    }
                }
                StringBuilder text = new StringBuilder(string_sortie.toString());
                text.replace(text.length() - 3, text.length() - 1, "");
                System.out.println(text + "ont des prénoms identiques");
            }
        }
        else {
            System.out.println("Il n'y a personne avec le même prénom qu'un/une autre");
        }
    }

    static void meme_nom(ArrayList<ArrayList<Object>> liste_personnes){
        ArrayList<String> liste_noms = new ArrayList<>();
        Set<String>set_noms = new HashSet<>();
        ArrayList<String> noms_en_double = new ArrayList<>();
        for (ArrayList<Object> p : liste_personnes){
            liste_noms.add(p.get(1).toString());
        }
        for (String p : liste_noms){
            if (!set_noms.add(p)) {
                noms_en_double.add(p);
            }
        }
        if (noms_en_double.size()>0) {
            for (String p : noms_en_double) {
                StringBuilder string_sortie = new StringBuilder(new String());
                for (ArrayList<Object> q : liste_personnes) {
                    if (Objects.equals(q.get(1).toString(), p)) {
                        string_sortie.append(q).append(" et ");
                    }
                }
                StringBuilder text = new StringBuilder(string_sortie.toString());
                text.replace(text.length() - 3, text.length() - 1, "");
                System.out.println(text + "ont des noms identiques");
            }
        }
        else {
            System.out.println("Il n'y a personne avec le même nom qu'un/une autre");
        }
    }

    static void meme_age(ArrayList<ArrayList<Object>> liste_personnes){
        ArrayList<String> liste_ages = new ArrayList<>();
        Set<String>set_ages = new HashSet<>();
        ArrayList<String> ages_en_double = new ArrayList<>();
        for (ArrayList<Object> p : liste_personnes){
            liste_ages.add(p.get(2).toString());
        }
        for (String p : liste_ages){
            if (!set_ages.add(p)) {
                ages_en_double.add(p);
            }
        }
        if (ages_en_double.size()>0) {
            for (String p : ages_en_double) {
                StringBuilder string_sortie = new StringBuilder(new String());
                for (ArrayList<Object> q : liste_personnes) {
                    if (Objects.equals(q.get(2).toString(), p)) {
                        string_sortie.append(q).append(" et ");
                    }
                }
                StringBuilder text = new StringBuilder(string_sortie.toString());
                text.replace(text.length() - 3, text.length() - 1, "");
                System.out.println(text + "ont des ages identiques");
            }
        }
        else {
            System.out.println("Il n'y a personne avec le même nage qu'un/une autre");
        }
    }

    public static void main(String[] args) {

        ArrayList<Object> personne1 = new ArrayList<>(Arrays.asList("Pascal", "ROSE", 43));
        ArrayList<Object> personne2 = new ArrayList<>(Arrays.asList("Mickael", "FLEUR", 29));
        ArrayList<Object> personne3 = new ArrayList<>(Arrays.asList("Henri", "TULIPE", 35));
        ArrayList<Object> personne4 = new ArrayList<>(Arrays.asList("Michel", "FRAMBOISE", 35));
        ArrayList<Object> personne5 = new ArrayList<>(Arrays.asList("Arthur", "PETALE", 35));
        ArrayList<Object> personne6 = new ArrayList<>(Arrays.asList("Michel", "POLLEN", 50));

        ArrayList<ArrayList<Object>> liste_personnes = new ArrayList<>();

        liste_personnes.add(personne1);
        liste_personnes.add(personne2);
        liste_personnes.add(personne3);
        liste_personnes.add(personne4);
        liste_personnes.add(personne5);
        liste_personnes.add(personne6);

        Collections.shuffle(liste_personnes);
        System.out.println(liste_personnes);
        camping(liste_personnes);
        meme_nb_lettres_nom_prenom(liste_personnes);
        meme_prenom(liste_personnes);
        meme_nom(liste_personnes);
        meme_age(liste_personnes);

    }
}
