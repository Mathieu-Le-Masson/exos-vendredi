package tri_de_liste;

import java.util.*;

public class tri_de_listes {

    public record Personne(String prenom, String nom, int age) {

        @Override
        public String toString() {
            return (this.prenom() +
                    " " + this.nom() +
                    " " + this.age() + " ans");
        }
    }


    public static void main(String[] args){
        ArrayList<Personne> liste_personnes = new ArrayList<>();

        liste_personnes.add(new Personne("Pascal", "ROSE", 43));
        liste_personnes.add(new Personne("Mickael", "FLEUR", 29));
        liste_personnes.add(new Personne("Henri", "TULIPE", 35));
        liste_personnes.add(new Personne("Micheal", "FRAMBOISE", 35));
        liste_personnes.add(new Personne("Arthur", "PETAL", 35));
        liste_personnes.add(new Personne("Michel", "POLLEN", 50));

        Collections.shuffle(liste_personnes);

        liste_personnes.sort(Comparator.comparing(Personne::nom));
        System.out.println("## Liste 1 : tri tri par ordre alphabétique des noms ##");
        liste_personnes.forEach(System.out::println);
        System.out.println("## FIN ##");
        System.out.println();

        liste_personnes.sort(Comparator.comparing(Personne::prenom).thenComparing(Personne::nom));
        System.out.println("## Liste 2 : tri tri par ordre alphabétique des prenoms ##");
        liste_personnes.forEach(System.out::println);
        System.out.println("## FIN ##");
        System.out.println();

        liste_personnes.sort(Comparator.comparing(Personne::age).thenComparing(Personne::prenom));
        System.out.println("## Liste 3 : tri tri par ordre alphabétique des ages ##");
        liste_personnes.forEach(System.out::println);
        System.out.println("## FIN ##");
        System.out.println();

        liste_personnes.sort(Comparator.comparing(Personne::age).reversed().thenComparing(Personne::prenom));
        System.out.println("## Liste 4 : tri tri par ordre alphabétique des ages ##");
        liste_personnes.forEach(System.out::println);
        System.out.println("## FIN ##");
    }
}
