package tri_de_liste;

public record Personne(String prenom, String nom, int age) {

    @Override
    public String toString() {
        return (this.prenom() +
                " " + this.nom() +
                " " + this.age() + " ans");
    }
}

