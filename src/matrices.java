import java.util.*;

class MatriceDimensionError extends Exception {
    public MatriceDimensionError(){
        System.out.println("Les matrices ne sont pas de dimensions compatibles pour cette opération");
    }
}


public class matrices {
    static ArrayList<ArrayList<Integer>> matrice(int nb_lignes, int nb_colonnes){
        ArrayList<ArrayList<Integer>> matrice = new ArrayList<>();
        for (int i=0; i<nb_lignes; i++){
            ArrayList<Integer> ligne = new ArrayList<>();
            for (int j=0; j<nb_colonnes; j++) {
                ligne.add(0);
            }
            matrice.add(ligne);
        }
        return matrice;
    }

    static ArrayList<ArrayList<Double>> matrice_double(int nb_lignes, int nb_colonnes){
        ArrayList<ArrayList<Double>> matrice = new ArrayList<>();
        for (int i=0; i<nb_lignes; i++){
            ArrayList<Double> ligne = new ArrayList<>();
            for (int j=0; j<nb_colonnes; j++) {
                ligne.add(0.0);
            }
            matrice.add(ligne);
        }
        return matrice;
    }

    static ArrayList<ArrayList<Integer>> matrice_a_remplir(){
        Scanner scan = new Scanner(System.in);
        System.out.println("Combien de lignes" );
        int nb_lignes = scan.nextInt();
        System.out.println("Combien de colonnes" );
        int nb_colonnes = scan.nextInt();
        ArrayList<ArrayList<Integer>> matrice_remplie = new ArrayList<>();
        for (int i=0; i<nb_lignes; i++){
            ArrayList<Integer> ligne = new ArrayList<>();
            for (int j=0; j<nb_colonnes; j++) {
                System.out.println("Enter valur for line "+ (i + 1) +" and column "+(j+1) );
                int valeur = scan.nextInt();
                ligne.add(valeur);
            }
            matrice_remplie.add(ligne);
        }
        return matrice_remplie;
    }

    static ArrayList<ArrayList<Integer>> change_valeur(ArrayList<ArrayList<Integer>> matrice, int ligne, int colonne, int nouvelle_valeur) {
        matrice.get(ligne - 1).set(colonne - 1, nouvelle_valeur) ;
        return matrice;
    }

    static ArrayList<ArrayList<Integer>> matrice_identite(int taille_matrice){
        ArrayList<ArrayList<Integer>> matrice_identite = matrice(taille_matrice, taille_matrice);
        for (int ligne =1; ligne < matrice_identite.size()+1; ligne++){
            change_valeur(matrice_identite, ligne, ligne, 1);
        }
        return matrice_identite;
    }

    static void print_matrice(ArrayList<ArrayList<Integer>> matrice){
        for (ArrayList<Integer> i : matrice) {
            System.out.println(i);
        }
    }

    static void print_matrice_double(ArrayList<ArrayList<Double>> matrice){
        for (ArrayList<Double> i : matrice) {
            System.out.println(i);
        }
    }

    static boolean dimension_check(ArrayList<ArrayList<Integer>> matrice1, ArrayList<ArrayList<Integer>> matrice2){
        boolean meme_dimension = true;
        if (matrice1.size() != matrice2.size()){
            meme_dimension = false;
        }
        else{
            if (matrice1.get(0).size() != matrice2.get(0).size()){
                meme_dimension = false;
            }
        }
        return meme_dimension;
    }

    static ArrayList<ArrayList<Integer>> somme_matrices(ArrayList<ArrayList<Integer>> matrice1, ArrayList<ArrayList<Integer>> matrice2)
            throws MatriceDimensionError {
        if (!dimension_check(matrice1, matrice2)) {
            throw new MatriceDimensionError();
        }
        else {
            ArrayList<ArrayList<Integer>> matrice_somme = matrice(matrice1.size(), matrice1.get(0).size());
            if (matrice_somme.size() > 0 && matrice1.size() > 0 && matrice2.size() > 0) {
                for (int i = 0; i < matrice1.size(); i++) {
                    for (int j = 0; j < matrice1.get(0).size(); j++) {
                        matrice_somme.get(i).set(j, matrice1.get(i).get(j) + matrice2.get(i).get(j));
                    }
                }
            }
            return matrice_somme;
        }
    }

    static ArrayList<ArrayList<String>> is_element(ArrayList<ArrayList<Integer>> matrice, int nombre) {
        ArrayList<ArrayList<String>> liste_positions = new ArrayList<>();
        for (int i = 0; i < matrice.size(); i++) {
            for (int j = 0; j < matrice.get(0).size(); j++) {
                if (matrice.get(i).get(j) == nombre) {
                    ArrayList<String> position = new ArrayList<>();
                    position.add(String.valueOf(i));
                    position.add(String.valueOf(j));
                    liste_positions.add(position);
                }
            }
        }
        if (liste_positions.isEmpty()) {
            System.out.println("Ce nombre ne se trouve pas dans la matrice");
        }
        return liste_positions;
    }

    static ArrayList<ArrayList<Double>> produit_scalaire(ArrayList<ArrayList<Integer>> matrice, double nombre) {
        ArrayList<ArrayList<Double>> produit_scalaire = matrice_double(matrice.size(), matrice.get(0).size());
        if (matrice.size() > 0) {
            for (int i = 0; i < matrice.size(); i++) {
                for (int j = 0; j < matrice.get(0).size(); j++) {
                    produit_scalaire.get(i).set(j, Double.valueOf(matrice.get(i).get(j)) * nombre);
                }
            }
        }
        return produit_scalaire;
    }

    static ArrayList<ArrayList<Integer>> produit_matriciel(ArrayList<ArrayList<Integer>> matrice1, ArrayList<ArrayList<Integer>> matrice2)
            throws MatriceDimensionError {
        if (!(matrice1.get(0).size() == matrice2.size())) {
            throw new MatriceDimensionError();
        }
        else {
            ArrayList<ArrayList<Integer>> produit_matriciel = matrice(matrice1.size(), matrice2.get(0).size());
            for (int i = 0; i < matrice1.size(); i++) {
                for (int j = 0; j < matrice2.get(0).size(); j++) {
                    int somme = 0;
                    for (int k = 0; k < matrice2.size(); k++) {
                        somme += matrice1.get(i).get(k) * matrice2.get(k).get(j);
                    }
                    produit_matriciel.get(i).set(j, somme);
                }
            }
            return produit_matriciel;
        }
    }

    public static void main(String[] args) throws MatriceDimensionError {

        ArrayList<ArrayList<Integer>> matrice = matrice(4, 3);
        System.out.println(change_valeur(matrice, 3,2, 6));
        print_matrice(matrice_identite(4));
        System.out.println(dimension_check(matrice_identite(4), matrice));
        ArrayList<ArrayList<Integer>> matrice1 = new ArrayList<>();
        ArrayList<Integer> ligne1= new ArrayList<>(Arrays.asList(6, 7, 9));
        ArrayList<Integer> ligne2= new ArrayList<>(Arrays.asList(2, 12, 4));
        ArrayList<Integer> ligne3= new ArrayList<>(Arrays.asList(3, 2, 1));
        matrice1.add(ligne1); matrice1.add(ligne2); matrice1.add(ligne3);
        System.out.println(matrice1);
        ArrayList<ArrayList<Integer>> matrice2 = new ArrayList<>();
        ArrayList<Integer> ligne4= new ArrayList<>(Arrays.asList(2, 2, 1));
        ArrayList<Integer> ligne5= new ArrayList<>(Arrays.asList(1, 4, 5));
        ArrayList<Integer> ligne6= new ArrayList<>(Arrays.asList(0, 2, 1));
        matrice2.add(ligne4); matrice2.add(ligne5); matrice2.add(ligne6);
        System.out.println(matrice2);
        System.out.println(somme_matrices(matrice1, matrice2));
        System.out.println(is_element(matrice2, 1));
        print_matrice(produit_matriciel(matrice1, matrice2));
    }
}