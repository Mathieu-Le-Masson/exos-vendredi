import java.io.*;
import java.util.*;

public class Espion {

    /**
     * The print_liste function prints out the list of lists.*
     *
     * @param liste Store the list of lists that are returned by the function
     *
     * @docauthor Trelent
     */
    public static void print_liste(List<List<String>> liste){
        for (List<String> i : liste) {
            System.out.println(i);
        }
    }


    /**
     * The read_csv_file function reads a csv file and returns a list of lists.*
     *
     * @param file_path Specify the file path to the csv file
     *
     * @return A list of lists
     *
     * @docauthor Trelent
     */
    public static List<List<String>> read_csv_file(String file_path){
        List<List<String>> records = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(file_path))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(",");
                records.add(Arrays.asList(values));
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return records;
    }

    /**
     * The creer_hasmap_nom function creates a HashMap of names and their associated
     * values. The function takes in a list of lists, where each sublist contains the
     * name, age and gender of an agent. It then iterates through this list to create
     * the HashMap with keys being the name and values being arrays containing age and gender.


     *
     * @param liste_espions Create a hashmap of the names of the spies
     *
     * @return A hashmap of the names and the associated agents
     *
     * @docauthor Trelent
     */
    public static HashMap<String, ArrayList<String >> creer_hasmap_nom(List<List<String>> liste_espions){

        HashMap<String, ArrayList<String >> noms = new HashMap<>();
        for (List<String > i : liste_espions) {
            ArrayList<String> nom = new ArrayList<>();
            noms.put(i.get(0), nom);
            noms.get(i.get(0)).add(i.get(1));
            noms.get(i.get(0)).add(i.get(2));
        }
        return noms;
    }

    /**
     * The creer_hasmap_voyages function creates a HashMap of voyages.*
     *
     * @param liste_espions Store the list of lists that contains all the information about each spy
     *
     * @return A hashmap with the following structure:
     *
     * @docauthor Trelent
     */
    public static HashMap<String, ArrayList<String >> creer_hasmap_voyages(List<List<String>> liste_espions){
        HashMap<String, ArrayList<String >> voyages = new HashMap<>();
        for (List<String > i : liste_espions) {
            ArrayList<String> voyage = new ArrayList<>();
            voyages.put(i.get(0), voyage);
            for (String s : i.get(3).split("/")) {
                voyages.get(i.get(0)).add(s);
            }
        }
        return voyages;
    }

    /**
     * The creer_hasmap_langues function creates a HashMap of languages that each spy knows.*
     *
     * @param liste_espions Create a hashmap of the espions
     *
     * @return A hashmap that contains the name of each spy as a key, and an arraylist of languages they speak as the value
     *
     * @docauthor Trelent
     */
    public static HashMap<String, ArrayList<String >> creer_hasmap_langues(List<List<String>> liste_espions){
        HashMap<String, ArrayList<String >> langues = new HashMap<>();
        for (List<String > i : liste_espions) {
            ArrayList<String> voyage = new ArrayList<>();
            langues.put(i.get(0), voyage);
            for (String s : i.get(4).split("/")) {
                langues.get(i.get(0)).add(s);
            }
        }
        return langues;
    }

    /**
     * The prenom_memes_lettres function takes a list of lists of strings and returns a list
     * of lists containing the names that have the same letters as the given name.*
     *
     * @param liste_espions Store the list of lists that contains all the information about each spy
     * @param prenom_cherche The firstname that we want to compare our list to
     *
     * @return A list of suspects who have the same letters in their first name as the suspect we are looking for
     *
     * @docauthor Trelent
     */
    public static ArrayList<ArrayList<String>> prenom_memes_lettres(List<List<String>> liste_espions, String prenom_cherche){
        HashMap<String, ArrayList<String >> noms = creer_hasmap_nom(liste_espions);
        ArrayList<ArrayList<String >> liste_suspects = new ArrayList<>();
        for (List<String> s : liste_espions) {
            String nom_provisoire = noms.get(s.get(0)).get(0).toLowerCase();
            prenom_cherche = prenom_cherche.toLowerCase();
            if(nom_provisoire.length() == prenom_cherche.length()) {
                // convert strings to char array
                char[] charArray1 = nom_provisoire.toCharArray();
                char[] charArray2 = prenom_cherche.toCharArray();
                // sort the char array
                Arrays.sort(charArray1);
                Arrays.sort(charArray2);
                // if sorted char arrays are same
                // then the string is anagram
                if (Arrays.equals(charArray1, charArray2)){
                    liste_suspects.add(noms.get(s.get(0)));
                }
            }
        }
        if (liste_suspects.size() == 0){
            System.out.println( "Il n'y a personne de suspect dans cette liste.");}

        return liste_suspects;
    }



    /**
     * The remove_monolangue function removes all the lists from liste_espions that have only one language in them.*
     *
     * @param liste_espions Store the list of lists that will be used to create a hashmap
     *
     * @docauthor Trelent
     */
    public static void remove_monolangue(List<List<String>> liste_espions){
        HashMap<String, ArrayList<String >> langues = creer_hasmap_langues(liste_espions);
        liste_espions.removeIf(s -> langues.get(s.get(0)).size() <= 1);
    }



    /**
     * The remove_jamais_europe function removes all the lists from liste_espions that do not contain Europe in their voyages.
     *
     * @param liste_espions Store the list of lists that contains all the information about each spy
     *
     * @docauthor Trelent
     */
    public static void remove_jamais_europe(List<List<String>> liste_espions) {
        HashMap<String, ArrayList<String>> voyages = creer_hasmap_voyages(liste_espions);
        liste_espions.removeIf(s -> !voyages.get(s.get(0)).contains("Europe"));
    }


    public static void main(String[] args){

        read_csv_file("C:\\Users\\mathieu.le-masson\\Downloads\\Espions.csv");
        List<List<String>> liste_espions = read_csv_file("C:\\Users\\mathieu.le-masson\\Downloads\\Espions.csv");
        liste_espions.remove(0);

        System.out.println();
        remove_monolangue(liste_espions);
        System.out.println("Liste après suppression de tous ceux qui ne parlent qu’une seule langue.");
        print_liste(liste_espions);
        System.out.println();
        remove_jamais_europe(liste_espions);
        System.out.println("Liste après suppression de tous ceux qui n’ont pas visité l’Europe.");
        print_liste(liste_espions);
        System.out.println();
        System.out.println("S'il y a des suspects dans la liste, les voilà : " + prenom_memes_lettres(liste_espions, "Marc"));
    }
}
